
import PopupLoader from './popupLoader'

window.vitrineApp.clients.get(token).publicLibraries.popup = new PopupLoader
window.vitrineApp.clients.get(token).publicLibraries.popup.init()