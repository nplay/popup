
import HomePopup from './model/popup/default/homePopup'
import OfferTimePopup from './model/popup/default/offerTimePopup'
import HistoryRelatedProductPopup from './model/popup/default/historyRelatedProductPopup'
import NewsProductPopup from './model/popup/default/newsProductPopup';
import WeatherProductPopup from './model/popup/default/weatherProductPopup'

import WeatherProductPreviewPopup from './model/popup/preview/weatherProduct/popup';
import HistoryRelatedProductPreviewPopup from './model/popup/preview/historyRelatedProduct/popup'
import OfferTimePreviewPopup from './model/popup/preview/offerTime/popup'
import NewsProductPreviewPopup from './model/popup/preview/news/popup'

class InvokePopup
{
   constructor()
   {
      this.apiManager = new window.vitrineApp.publicClasses.loader.APIManager();
      this.popupAPI = new this.apiManager.themes.popupAPI(token, true)

      this.popupClasses = new Object
   }

   _getPopupClass(className, popupEntity)
   {
      let types = new Object({
         home: HomePopup,
         related_product_history: HistoryRelatedProductPopup,
         offer_time: OfferTimePopup,
         newsletter: NewsProductPopup,
         weather_product: WeatherProductPopup,
         weather_product_preview: WeatherProductPreviewPopup,
         related_product_history_preview: HistoryRelatedProductPreviewPopup,
         offer_time_preview: OfferTimePreviewPopup,
         newsletter_preview: NewsProductPreviewPopup
      })

      if(!types.hasOwnProperty(className))
         throw new Error(`Class ${className} not exists PopupClass specialized`)

      return new types[className](popupEntity)
   }

   async getBy(filter = {
      page: null,
      action: null
   })
   {
      let sessionID = await window.vitrineApp.clients.get(token).user.sessionID

      let themes = await this._loadThemes(filter)
      let collection = new Array

      for(let theme of themes)
      {
         let cacheKey = `vitrineApp_popup_${sessionID}_${theme._id}`
         let popupCache = new window.vitrineApp.publicClasses.loader.CacheHelper(cacheKey)

         if(popupCache.hasCached())
            continue;

         try
         {
            collection.push(await this.get(theme))
            popupCache.setCache(true)
         } catch (error) {
            console.error(`Fail load popup theme ${error.toString()}`)
         }
      }

      return collection
   }

   async get(theme, forceReload = false)
   {
      if(forceReload || !this.popupClasses.hasOwnProperty(theme._id))
         this.popupClasses[theme._id] = this._getPopupClass(theme.type, theme)

      return this.popupClasses[theme._id]
   }

   async _loadThemes(filter = {
      page: null,
      action: null
   })
   {
      let body = {
         filters: {
            "enabled": true,
            "page": ["all"]
         }
      }

      if(filter.page)
         body.filters.page.push(filter.page)
      
      if(filter.action)
         body.filters.action = filter.action

      let response = await this.popupAPI.get(body)
      let themes = response.data.data
      return themes
   }
}

export default InvokePopup