
import Mustache from 'mustache'

class Data
{
    constructor()
    {
        this.client = window.vitrineApp.clients.get(token)
        this.user = this.client.user
    }

    async populateData(data)
    {
        this.data = data

        await this.city()
        await this.username()
        await this.temperature()
        await this.headerMessage()

        return this
    }

    async headerMessage()
    {
        let isNewUser = !await this.username()
        let titleFlag = isNewUser ? 'newUser' : 'recurrencyUser'
        let titleMessage = this.data.titles[titleFlag]

        this.data.header_message = Mustache.render(titleMessage, this.data)

        return this.data.header_message
    }

    async city()
    {
        let curWeather = await this.user.data.weather.get()
        this.data.city = curWeather.city || null

        return this.data.city
    }

    async temperature()
    {
        let curWeather = await this.user.data.weather.get()  || {}
        this.data.temperature = curWeather.temp || null

        return this.data.temperature
    }

    async username()
    {
        let userData = await this.user.data.get() || {}
        this.data.username = userData.name || null
        
        return this.data.username
    }
}

export default Data