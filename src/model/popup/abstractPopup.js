import SwiperPlugin from "../plugin/swiper/plugin"

class AbstractPopup
{
   constructor(popupEntity)
   {
      this.popupEntity = popupEntity
      this.render = null
      this.eventTarget = new EventTarget()
   }

   async _afterPrepareDataEvent()
   {
      let event = new CustomEvent("afterPrepareData", {
         detail: {
            popupEntity: this.popupEntity
         }
      });

      this.eventTarget.dispatchEvent(event)
   }

   async _beforePrepareRenderEvent()
   {
      let event = new CustomEvent("beforePrepareRender", {
         detail: {
            popupEntity: this.popupEntity
         }
      });

      this.eventTarget.dispatchEvent(event)
   }

   async _afterPrepareRenderEvent()
   {
      let event = new CustomEvent("afterPrepareRender", {
         detail: {
            render: this.render
         }
      });

      this.eventTarget.dispatchEvent(event)
   }

   async initialize()
   {
      try
      {
         await this._prepareData()
         await this._afterPrepareDataEvent()

         await this._prepareSwiper()
         await this._hidrate()
         
         await this._beforePrepareRenderEvent()
         await this._prepareRender()
         await this._afterPrepareRenderEvent()

         await this._runRender()
      }
      catch (error)
      {
         console.error(`Fail in load popup`, error)
      }
   }

   async _prepareData(){}
   async _isQualifiedRender(){}
   async _hidrate(){}
   async _prepareRender(){}

   async _prepareSwiper()
   {
      this.popupEntity.additionalData.swiperPlugin = new SwiperPlugin(this.popupEntity)
   }

   timeout(ms = 1000)
   {
      return new Promise((resolve, reject) => {
         setTimeout(() => {
            resolve(true)
         }, ms);
      })
   }

   async _runRender()
   {
      await this.timeout(this.popupEntity.additionalData.render.delayMS)
      return await this._isQualifiedRender()
   }
}

export default AbstractPopup