import NewsProductPopup from '../../default/newsProductPopup'

class Popup extends NewsProductPopup
{
   constructor(popupEntity)
   {
      super(popupEntity);
   }

   async _prepareData()
   {
      await this._setCSSDefault()
   }

   async _setCSSDefault()
   {
      let HTML = decodeURIComponent(window.atob(this.popupEntity.html))
      let doc = new DOMParser().parseFromString(HTML, 'text/html').body
      let style = document.createElement('style')
      style.innerHTML = require('../../../../assets/css/defaultPopup.css').toString()
      doc.appendChild(style)
      this.popupEntity.html = window.btoa(encodeURIComponent(doc.innerHTML))
   }

   async _isQualifiedRender()
   {
      return true
   }

   async _hidrate()
   {
      this.popupEntity.additionalData.header_message = this.popupEntity.additionalData.titles.newUser
      this.popupEntity.additionalData.useSwiper = true
      await this._setItens()
   }
}

export default Popup