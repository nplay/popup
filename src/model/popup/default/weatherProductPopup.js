import AbstractPopup from "../abstractPopup";
import Render from "../../render";
import Data from '../../visualEditor/data'
import ArrayHelper from '../../../helper/array'
import Condition from '../../condition'
import AnalyticsPlugin from "../../plugin/analytics/plugin";

class WeatherProductPopup extends AbstractPopup
{
   constructor(popupEntity)
   {
      super(popupEntity);
   }

   async _prepareData()
   {
      this.popupEntity.additionalData.itens = new Array
   }

   async _hidrate()
   {
      let dataHelper = new Data()
      await dataHelper.populateData(this.popupEntity.additionalData)

      this.popupEntity.additionalData.useSwiper = true
      await this._setItens()
   }

   async _prepareRender()
   {
      this.render = new Render(this.popupEntity)

      if(this.popupEntity.additionalData.useSwiper)
         this.render.addPlugin(this.popupEntity.additionalData.swiperPlugin, 'swiperPlugin')

      this.render.addPlugin(new AnalyticsPlugin(this.popupEntity), 'analyticsPlugin')
   }

   async _isQualifiedRender()
   {
      let itensQtyValid = this.popupEntity.additionalData.itens.length > 0
      let condition = new Condition(this.popupEntity.additionalData.climate.rules, this.popupEntity.additionalData)

      let condValid = await condition.validate()
      return itensQtyValid && condValid;
   }

   async _runRender()
   {
      let canRun = await super._runRender()

      if(!canRun)
         return this;

      return await this.render.initialize()
   }

   async _setItens()
   {
      if(this.popupEntity.additionalData.itens.length == 0)
         this.popupEntity.additionalData.itens = await this._getItens();

      this.popupEntity.additionalData.itens = this.popupEntity.additionalData.itens
   }

   /**
    * Retrive history itens by session ID
    */
   async _getItens()
   {
      let apiManager = new window.vitrineApp.publicClasses.loader.APIManager()
      
      let rule = this.popupEntity.additionalData.search.rules[0] || null

      if(!rule)
         return new Array

      let availableAPIs = new Object({
         'catBestSeller': 'BestSellerItemAPI',
         'catNew': 'NewItemAPI',
         'catPromotional': 'PromotionalItemAPI'
      })

      let invokeAPI = availableAPIs.hasOwnProperty(rule.variable) ? availableAPIs[rule.variable] : null

      if(!invokeAPI)
         return new Array

      let categoryIDs = rule.value.split(',')

      let api = new apiManager.searches[invokeAPI](token, true)
      let response = await api.byCategorys.get(categoryIDs)

      if(this.popupEntity.additionalData.randomItens)
         return new ArrayHelper().shuffle(response.data.data)
      
      return response.data.data
   }
}

export default WeatherProductPopup