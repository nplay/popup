import AbstractPopup from "./../abstractPopup";

class HomePopup extends AbstractPopup
{
   constructor(popupEntity)
   {
      super(popupEntity);
   }
}

export default HomePopup