import AbstractPopup from "../abstractPopup";
import Render from "../../render";
import CupomPlugin from "./offerTime/plugin/cupomPlugin";
import Data from "../../visualEditor/data";

class OfferTimePopup extends AbstractPopup
{
   constructor(popupEntity)
   {
      super(popupEntity);
   }

   async _prepareData(){
   }

   async _isQualifiedRender(){
      return this.popupEntity.additionalData.couponCode != ""
   }

   async _hidrate()
   {
      let dataHelper = new Data
      await dataHelper.populateData(this.popupEntity.additionalData)
   }

   async _prepareRender(){
      this.render = new Render(this.popupEntity)
      let cupomPlugin = new CupomPlugin(this);

      this.render.addPlugin(cupomPlugin, 'cupomPlugin')
   }

   async _runRender(){
      let canRun = await super._runRender()

      if(!canRun)
         return this;

      return this.render.initialize()
   }
}

export default OfferTimePopup