import AbstractPlugin from "../../../../plugin/abstract";

class CupomPlugin extends AbstractPlugin
{
   constructor(data)
   {
      super(data);
   }

   get showButtonCupom()
   {
      let popupID = '#'+this.data.render.popupID
      let button = document.querySelector(`${popupID} #showCode`)
      return button
   }

   get codeInputCupom()
   {
      let popupID = '#'+this.data.render.popupID
      let element = document.querySelector(`${popupID} #code`)

      return element
   }

   get identifier(){
      return this.data._id;
   }

   run(){
      var input = this.codeInputCupom
      var btnEle = this.showButtonCupom

      if(!btnEle || !input)
         return false;

      btnEle.addEventListener('click', (evt) => {
         input.style.display = 'block';
         btnEle.style.display = 'none';
      })
   }
}

export default CupomPlugin