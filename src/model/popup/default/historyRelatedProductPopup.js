import AbstractPopup from "../abstractPopup";
import Render from "../../render";
import Data from "../../visualEditor/data";
import AnalyticsPlugin from "../../plugin/analytics/plugin";

class HistoryRelatedProductPopup extends AbstractPopup
{
   constructor(popupEntity)
   {
      super(popupEntity);
   }

   async _prepareData()
   {
      this.popupEntity.additionalData.itens = new Array
   }

   async _hidrate()
   {
      let dataHelper = new Data
      await dataHelper.populateData(this.popupEntity.additionalData)

      this.popupEntity.additionalData.useSwiper = true
      await this._setItens()
   }

   async _prepareRender()
   {
      this.render = new Render(this.popupEntity)

      if(this.popupEntity.additionalData.useSwiper)
         this.render.addPlugin(this.popupEntity.additionalData.swiperPlugin, 'swiperPlugin')

      this.render.addPlugin(new AnalyticsPlugin(this.popupEntity), 'analyticsPlugin')
   }

   async _isQualifiedRender()
   {
      return this.popupEntity.additionalData.itens.length > 0;
   }

   async _runRender()
   {
      let canRun = await super._runRender()

      if(!canRun)
         return this;

      return await this.render.initialize()
   }

   async _setItens()
   {
      if(this.popupEntity.additionalData.itens.length == 0)
         this.popupEntity.additionalData.itens = await this._getItens()
   }

   /**
    * Retrive history itens by session ID
    */
   async _getItens()
   {
      let sessionID = await window.vitrineApp.clients.get(token).user.sessionID

      if(!sessionID)
         return new Array

      let apiManager = new window.vitrineApp.publicClasses.loader.APIManager()
      let historyItemAPI = new apiManager.searches.HistoryItemAPI(token, true)
      
      let response = await historyItemAPI.get(sessionID)
      return response.data.data
   }
}

export default HistoryRelatedProductPopup