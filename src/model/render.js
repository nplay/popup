import RenderEngine from './render/engine'

class Render {
   constructor(popupEntity) {

      this.options = {containerXpath: 'body'}
      this.popupEntity = popupEntity
      this.popupID = `_${this.popupEntity._id}`
      this.data = Object.assign(this.popupEntity.additionalData, {popupID: this.popupID})
      this.renderEngine = null

      //Inject plugins to work in render class
      this.plugins = new Object
   }

   async setElementContainer()
   {
      this._elementContainer = this._createElement(this.options.containerXpath)
   }

   async getElementContainer()
   {
      if(!this._elementContainer)
         await this.setElementContainer()

      return this._elementContainer
   }

   async initialize()
   {
      await this.initRender()

      await this.callPlugins('mountBefore')
      
      await this.renderEngine.before()
      let elementContainer = await this.getElementContainer()
      elementContainer.appendChild(await this.renderEngine.after())

      await this.callPlugins('mountAfter')
      await this.callPlugins('run')
   }

   addPlugin(pluginInstance, identifier)
   {
      pluginInstance.addData('render', this)
      this.plugins[identifier] = pluginInstance
   }

   async callPlugins(method = 'run')
   {
      let keys = Object.keys(this.plugins)

      for(let identifier of keys)
         await this.plugins[identifier][method](this)
   }

   async initRender()
   {
      let element = document.getElementById(this.popupID)
      if(element)
         element.remove();

      this.renderEngine = new RenderEngine(this.popupEntity.html, this.data)
   }

   _createElement(appendXPath)
   {
      let elementContainerID = 'vitrineAppPopup'
      let element = this._querySelector("#"+elementContainerID);
      
      if(element)
         return element;

      element = document.createElement('div')
      element.setAttribute('ref', elementContainerID)
      element.setAttribute('id', elementContainerID)

      let documentAppend = this._querySelector(appendXPath)

      //documentAppend.insertBefore(element, documentAppend.firstChild);
      documentAppend.insertAdjacentElement('beforebegin', element)
      return element
   }

   _querySelector(elementSelector) {
      let ele = document.querySelector(elementSelector)
      return ele ? ele : null;
   }
}

export default Render