
import Vue from 'vue'
import component from "../../component/dynamic.vue"

class Engine
{
    constructor(html, data)
    {
      this.html = html
      this.data = data
      this.element = null
    }

    async _buildElement()
    {
       let parameters = await this._prepareParameters();

       let ComponentClass = Vue.extend(component)
       let element = new ComponentClass({
         data: () => { return parameters; }
       })

       return element
    }
 
    async _mountElement()
    {
      this.element.$mount();
      return this.element.$el
    }

    async _prepareParameters()
    {
       let templateData = {
         html: null,
         style: null,
         popupID: this.popupID,
         editEle: {},
       }
 
       templateData.html = this.decode(this.html);
       templateData.style = this.getTagStyles(templateData, true).join('\n')
       
       let data = Object.assign(templateData, this.data)
       this._data = data
 
       return this._data
    }

    getTagStyles(templateData, removeTagStyle)
    {
       let parser = new DOMParser();
       let body = parser.parseFromString(templateData.html, 'text/html').body
       let styles = body.querySelectorAll('style')
 
       let stylesHTML = new Array
 
       for (var i = 0; i < styles.length; ++i)
       {
          let node = styles[i];
          stylesHTML.push(node.innerHTML)
          node.remove()
       }
 
       if(removeTagStyle)
          templateData.html = body.innerHTML;
       
       return stylesHTML
    }

    async before()
    {
      this.element = await this._buildElement()
    }

    async after()
    {
      return await this._mountElement()
    }

    decode(data)
    {
       data = window.atob(data)
       return window.decodeURIComponent(data)
    }
}

export default Engine