
class Condition
{
    constructor(rules, data)
    {
        this.rules = rules
        this.data = data
    }

    async validate()
    {
        for(let rule of this.rules)
        {
            if(!await this[rule.operator](rule))
                return false
        }

        return this.rules.length > 0
    }

    async if(rule)
    {
        let variableVal = eval(`this.data.${rule.variable}`)
        let output = eval(`${variableVal} ${rule.condition} ${rule.value}`)

        return output
    }
}

export default Condition