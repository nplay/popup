
class AbstractPlugin {
   constructor(data) {
      this.data = data || new Object
   }

   async run()
   {
   }

   addData(key, value)
   {
      this.data[key] = value;
   }

   async mountBefore(renderInstance){}
   async mountAfter(renderInstance){}
}

export default AbstractPlugin