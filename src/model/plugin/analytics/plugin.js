
import AbstractPlugin from '../abstract'

class AnalyticsPlugin extends AbstractPlugin
{
    constructor(data)
    {
        super(data)
    }

    async mountBefore(renderInstance){}

    async mountAfter(renderInstance){

        let itens = renderInstance.data.itens
        
        let containerPopus = renderInstance._elementContainer
        let popupContainer = containerPopus.querySelector(`#${renderInstance.popupID}`)

        let analytics = window.vitrineApp.clients.get(token).publicLibraries.analytics
        
        for(let item of itens)
        {
            let itemElement = popupContainer.querySelector(`[data-entity-id="${item.entityId}"]`)

            if(!itemElement)
                continue;

            itemElement.addEventListener('click', (evt) => {
                let itens = [{
                    originCollection: [
                        {
                            "title": renderInstance.popupEntity.name,
                            "id": renderInstance.popupEntity._id,
                            "node": {
                                "id": "popup",
                                "name": "Popup"
                            }
                        }
                    ],
                    product: item
                }]
                
                analytics.track.prospectItem(itens)
            })
        }
    }
}

export default AnalyticsPlugin