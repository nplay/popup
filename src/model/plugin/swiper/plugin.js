
var SwiperLibrary = require('swiper');
import Resize from './on/resize'
import AbstractPlugin from '../abstract'

/**
 * MANAGE AND INITIALIZE SWIPER
 */
class SwiperPlugin extends AbstractPlugin
{
    constructor(data)
    {
        super(data)

        this.configuration = this._config()
        this.container = null;
        this._objSwiper = null;
    }

    mountAfter(renderInstance)
    {
        this.container = renderInstance.renderEngine.element.$el.querySelector('.swiper-container')

        this._run()
        return this
    }

    _applyCSS()
    {
       if(document.querySelector('style#swiper'))
          return;
 
       let style = document.createElement('style')
       style.id ="swiper"
       style.innerHTML = require('../../../assets/css/swiper.css').toString()
       document.body.appendChild(style)
    }

    _run()
    {
        this._applyCSS()
        this._objSwiper = new SwiperLibrary.default(this.container, this.configuration);
        window.swiper = this._objSwiper
    }

    _config(){
        var resize = new Resize()

        return {
            init: true,
            direction: 'horizontal',
            slidesPerView: 4,
            observer: true,
            observeParents: true,
            navigation: {
               prevEl: '',//`#${this.data.render.popupID} .swiper-button-prev`,
               nextEl: '',//`#${this.data.render.popupID} .swiper-button-next`,
            },
            autoplay: {
                delay: 3000,
            },
            on: {
                resize: resize.initialize.bind(resize, this),
                init: resize.initialize.bind(resize, this),
            }
         }
        
        /*return {
            init: true,
            direction: 'horizontal',
            simulateTouch: false,
            slidesPerGroup: 1,
            slidesPerColumnFill: "row",
            slidesPerColumn: 2,
            slidesPerView: 4,
            observer: true,
            observeParents: true,
            navigation: {
            prevEl: '',//`#${this.data.render.popupID} .swiper-button-prev`,
            nextEl: '',//`#${this.data.render.popupID} .swiper-button-next`,
            },
           on: {
            resize: resize.initialize.bind(resize, this),
            init: resize.initialize.bind(resize, this),
           }
        }*/
     }
}

export default SwiperPlugin