
class Resize
{
    initialize(swiper)
    {
        this.swManager = swiper

        if(typeof this.swManager.configuration.slidesPerView == 'number')
            this._resize()
    }

    _resize()
    {
        var me = this

        setTimeout(() => {
            me.swiper = this.swManager._objSwiper

            me._adjustSlidesPerView()
            me._adjustSlidesPerColumn()
            me.swiper.update(true);
        }, 50)
    }

    _adjustSlidesPerView()
    {
        let isColumn = this._columnsActive()
        if(isColumn)
            return false;
        
        let slideWidth = 250;
        let clientWidth = document.documentElement.clientWidth
        let defaultSlides = this.swiper.passedParams.slidesPerView
        
        let availableSlides = Math.floor(clientWidth / slideWidth)

        //console.log("Screen Width: " + clientWidth, 'Available Slides: ' + availableSlides)
        if(availableSlides >= defaultSlides)
            availableSlides = defaultSlides
        
        this.swiper.params.slidesPerView = availableSlides
    }

    _adjustSlidesPerColumn()
    {
        let isColumn = this._columnsActive()
        
        if(!isColumn)
            return false;

        let slideWidth = 250;
        let clientWidth = document.documentElement.clientWidth
        let defaultSlides = this.swiper.passedParams.slidesPerView
        
        let availableSlides = Math.floor(clientWidth / slideWidth)
        let slides = this.swiper.slides.length
        
        if(availableSlides >= defaultSlides)
            availableSlides = defaultSlides

        this.swiper.params.slidesPerView = availableSlides
        let slidesPerColumn = Math.ceil(slides / availableSlides)
        this.swiper.params.slidesPerColumn = slidesPerColumn

        return true
    }
    
    _columnsActive()
    {
        return this.swiper.passedParams.hasOwnProperty('slidesPerGroup')
    }
}

export default Resize