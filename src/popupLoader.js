
import InvokePopup from "./invokePopup";

class PopupLoader
{
   constructor()
   {
      this.invokePopup = new InvokePopup()
   }

   get options()
   {
      if(!this._options)
         this._options = JSON.parse(options)
      
      return this._options || {}
   }
   
   get mode()
   {
      return this.options.mode || 'live'
   }

   async init()
   {
      if(this.mode == 'live')
         await this.liveMode()
   }

   async liveMode()
   {
      let client = window.vitrineApp.clients.get(token)
      let clientAnalytics = client.publicLibraries.analytics

      let isMobile = await this.isMobile();

      if(isMobile)
      {
         let popups = await this.invokePopup.getBy({
            page: clientAnalytics.pageInformation.page()
         })

         popups.forEach((popup) => {
            popup.initialize()
         })
      }
      else
      {
         clientAnalytics.event.addListener('all', async (event) => {

            try {
               let popups = await this.invokePopup.getBy({
                  action: event.detail.action,
                  page: event.detail.page
               })
   
               popups.forEach((popup) => {
                  popup.initialize()
               })

            } catch (error) {
               console.warn('NPlay - Falha ao carregar os popups', error.toString())
               clientAnalytics.event.removeListener('all')
            }
         });
      }

      return this
   }

   async isMobile(){
      let client = window.vitrineApp.clients.get(token)
      return client.publicLibraries.analytics.pageInformation.isMobile()
   }

   async previewMode(popupEntity, functions = [])
   {
      popupEntity.type += '_preview'
      let popup = await this.invokePopup.get(popupEntity, true)

      for(let _func of functions)
         await _func(popupEntity)
      
      return popup
   }
}

export default PopupLoader